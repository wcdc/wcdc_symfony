<?php

namespace WCDC\ConceptBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Concept
 *
 * @ORM\Table(name="concept")
 * @ORM\Entity(repositoryClass="WCDC\ConceptBundle\Repository\ConceptRepository")
 */
class Concept
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     */
    private $title = "My title";

    /**
     * @var \stdClass
     *
     * @ORM\OneToOne(targetEntity="WCDC\ConceptBundle\Entity\Image", cascade={"persist", "remove"})
     * @Assert\Valid()
     */
    private $wcImg;

    /**
     * @var \stdClass
     *
     * @ORM\OneToOne(targetEntity="WCDC\ConceptBundle\Entity\Image", cascade={"persist", "remove"})
     * @Assert\Valid()
     */
    private $dcImg;

    /**
     * @var string
     *
     * @ORM\Column(name="wc_name", type="string", length=255)
     */
    private $wcName;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_name", type="string", length=255)
     */
    private $dcName;

    /**
     * @var string
     *
     * @ORM\Column(name="wc_content", type="text")
     */
    private $wcContent;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_content", type="text")
     */
    private $dcContent;

    /**
     * @var string
     *
     * @ORM\Column(name="wc_address", type="string", length=255, nullable=true)
     */
    private $wcAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="dc_address", type="string", length=255, nullable=true)
     */
    private $dcAddress;

    /**
     *
     * @ORM\ManyToOne(targetEntity="WCDC\CoreBundle\Entity\Cities", cascade={"persist"})
     */
    private $wcCity;

    /**
     *
     * @ORM\ManyToOne(targetEntity="WCDC\CoreBundle\Entity\Cities", cascade={"persist"})
     */
    private $dcCity;

    /**
     * @ORM\ManyToMany(targetEntity="WCDC\CoreBundle\Entity\Categories", cascade={"persist"})
     * @ORM\JoinTable(name="concept_categories")
     */
    private $categories;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_published", type="datetime")
     */
    private $datePublished;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_modified", type="datetime", nullable=true)
     */
    private $lastModified;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="author", type="object")
     */
    private $author;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="moderator", type="object", nullable=true)
     */
    private $moderator;

    /**
     * @var bool
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    private $locked;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_likes", type="integer")
     */
    private $nbLikes = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="nd_dislikes", type="integer")
     */
    private $ndDislikes = 0;



	public function __construct()
	{
		$this->locked = true;
		$this->datePublished    = new \Datetime();
		$this->categories       = new ArrayCollection();
	}

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Concept
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set wcImg
     *
     * @param \stdClass $wcImg
     *
     * @return Concept
     */
    public function setWcImg($wcImg)
    {
        $this->wcImg = $wcImg;

        return $this;
    }

    /**
     * Get wcImg
     *
     * @return \stdClass
     */
    public function getWcImg()
    {
        return $this->wcImg;
    }

    /**
     * Set dcImg
     *
     * @param \stdClass $dcImg
     *
     * @return Concept
     */
    public function setDcImg($dcImg)
    {
        $this->dcImg = $dcImg;

        return $this;
    }

    /**
     * Get dcImg
     *
     * @return \stdClass
     */
    public function getDcImg()
    {
        return $this->dcImg;
    }

    /**
     * Set wcName
     *
     * @param string $wcName
     *
     * @return Concept
     */
    public function setWcName($wcName)
    {
        $this->wcName = $wcName;

        return $this;
    }

    /**
     * Get wcName
     *
     * @return string
     */
    public function getWcName()
    {
        return $this->wcName;
    }

    /**
     * Set dcName
     *
     * @param string $dcName
     *
     * @return Concept
     */
    public function setDcName($dcName)
    {
        $this->dcName = $dcName;

        return $this;
    }

    /**
     * Get dcName
     *
     * @return string
     */
    public function getDcName()
    {
        return $this->dcName;
    }

    /**
     * Set wcContent
     *
     * @param string $wcContent
     *
     * @return Concept
     */
    public function setWcContent($wcContent)
    {
        $this->wcContent = $wcContent;

        return $this;
    }

    /**
     * Get wcContent
     *
     * @return string
     */
    public function getWcContent()
    {
        return $this->wcContent;
    }

    /**
     * Set dcContent
     *
     * @param string $dcContent
     *
     * @return Concept
     */
    public function setDcContent($dcContent)
    {
        $this->dcContent = $dcContent;

        return $this;
    }

    /**
     * Get dcContent
     *
     * @return string
     */
    public function getDcContent()
    {
        return $this->dcContent;
    }

    /**
     * Set wcAddress
     *
     * @param string $wcAddress
     *
     * @return Concept
     */
    public function setWcAddress($wcAddress)
    {
        $this->wcAddress = $wcAddress;

        return $this;
    }

    /**
     * Get wcAddress
     *
     * @return string
     */
    public function getWcAddress()
    {
        return $this->wcAddress;
    }

    /**
     * Set dcAddress
     *
     * @param string $dcAddress
     *
     * @return Concept
     */
    public function setDcAddress($dcAddress)
    {
        $this->dcAddress = $dcAddress;

        return $this;
    }

    /**
     * Get dcAddress
     *
     * @return string
     */
    public function getDcAddress()
    {
        return $this->dcAddress;
    }

    /**
     * Set wcCity
     *
     * @param string $wcCity
     *
     * @return Concept
     */
    public function setWcCity($wcCity)
    {
        $this->wcCity = $wcCity;

        return $this;
    }

    /**
     * Get wcCity
     *
     * @return string
     */
    public function getWcCity()
    {
        return $this->wcCity;
    }

    /**
     * Set dcCity
     *
     * @param string $dcCity
     *
     * @return Concept
     */
    public function setDcCity($dcCity)
    {
        $this->dcCity = $dcCity;

        return $this;
    }

    /**
     * Get dcCity
     *
     * @return string
     */
    public function getDcCity()
    {
        return $this->dcCity;
    }

	/**
	 * Add category
	 *
	 * @param \WCDC\CoreBundle\Entity\Categories $category
	 *
	 * @return Concept
	 */
	public function addCategory(\WCDC\CoreBundle\Entity\Categories $category)
	{
		$this->categories[] = $category;

		return $this;
	}

	/**
	 * Remove category
	 *
	 * @param \WCDC\CoreBundle\Entity\Categories $category
	 */
	public function removeCategory(\WCDC\CoreBundle\Entity\Categories $category)
	{
		$this->categories->removeElement($category);
	}

	/**
	 * Get categories
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getCategories()
	{
		return $this->categories;
	}

    /**
     * Set datePublished
     *
     * @param \DateTime $datePublished
     *
     * @return Concept
     */
    public function setDatePublished($datePublished)
    {
        $this->datePublished = $datePublished;

        return $this;
    }

    /**
     * Get datePublished
     *
     * @return \DateTime
     */
    public function getDatePublished()
    {
        return $this->datePublished;
    }

    /**
     * Set lastModified
     *
     * @param \DateTime $lastModified
     *
     * @return Concept
     */
    public function setLastModified($lastModified)
    {
        $this->lastModified = $lastModified;

        return $this;
    }

    /**
     * Get lastModified
     *
     * @return \DateTime
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * Set author
     *
     * @param \stdClass $author
     *
     * @return Concept
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \stdClass
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set moderator
     *
     * @param \stdClass $moderator
     *
     * @return Concept
     */
    public function setModerator($moderator)
    {
        $this->moderator = $moderator;

        return $this;
    }

    /**
     * Get moderator
     *
     * @return \stdClass
     */
    public function getModerator()
    {
        return $this->moderator;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     *
     * @return Concept
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return bool
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Set nbLikes
     *
     * @param integer $nbLikes
     *
     * @return Concept
     */
    public function setNbLikes($nbLikes)
    {
        $this->nbLikes = $nbLikes;

        return $this;
    }

    /**
     * Get nbLikes
     *
     * @return int
     */
    public function getNbLikes()
    {
        return $this->nbLikes;
    }

    /**
     * Set ndDislikes
     *
     * @param integer $ndDislikes
     *
     * @return Concept
     */
    public function setNdDislikes($ndDislikes)
    {
        $this->ndDislikes = $ndDislikes;

        return $this;
    }

    /**
     * Get ndDislikes
     *
     * @return int
     */
    public function getNdDislikes()
    {
        return $this->ndDislikes;
    }
}
