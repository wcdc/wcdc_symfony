<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 8/17/16
 * Time: 4:30 PM
 */

namespace WCDC\ConceptBundle\Form;

use WCDC\CoreBundle\Repository\CategoriesRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use WCDC\CoreBundle\Repository\CitiesRepository;

class ConceptType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		// Arbitrairement, on récupère toutes les catégories qui commencent par "D"

		$builder
			->add('wcImg',          ImageType::class, array('label' => 'Working image'))
			->add('dcImg',          ImageType::class, array('label' => 'Dying image'))
			->add('wcContent',      TextareaType::class, array('label' => 'Working content'))
			->add('dcContent',      TextareaType::class, array('label' => 'Dying content'))
			->add('wcName',         TextType::class, array('label' => 'Working name'))
			->add('dcName',         TextType::class, array('label' => 'Dying name'))
			->add('wcAddress',      TextType::class, array('label' => 'Working address', 'required' => false))
			->add('dcAddress',      TextType::class, array('label' => 'Dying address', 'required' => false))
			->add('wcCity',         EntityType::class,
				array(
						'class'         =>  'WCDCCoreBundle:Cities',
						'choice_label'  =>  'name',
						'label'         =>  'Working city',
						'placeholder'   =>  'Choose a working city*'
					))
			->add('dcCity',         EntityType::class,
				array(
						'class'         =>  'WCDCCoreBundle:Cities',
						'choice_label'  =>  'name',
						'label'         =>  'Dying city',
						'placeholder'   =>  'Choose a dying city*'
					))
			->add('categories',     EntityType::class,
				array(
						'class'         => 'WCDCCoreBundle:Categories',
						'choice_label'  => 'name',
						'multiple'      =>  true,
						'label'         =>  'Category',
						'placeholder'   =>  'Choose bettween 1 and 3 categories*'
				))
			->add('Submit',           SubmitType::class);
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'WCDC\ConceptBundle\Entity\Concept'
		));
	}
}