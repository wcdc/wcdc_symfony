<?php
// src/OC/PlatformBundle/Form/ImageType.php

namespace WCDC\ConceptBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('file', FileType::class, array('invalid_message' => 'Invalid file please retry'));
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
		  'data_class' => 'WCDC\ConceptBundle\Entity\Image'
		));
	}
}
