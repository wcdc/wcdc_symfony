<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 8/17/16
 * Time: 12:44 PM
 */

namespace WCDC\ConceptBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\Tests\Compiler\I;
use Symfony\Component\HttpFoundation\Request;
use WCDC\ConceptBundle\Entity\Concept;
use WCDC\ConceptBundle\Entity\Image;
use WCDC\ConceptBundle\Form\ConceptType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ConceptController extends Controller
{
	/**
	 * @Security("has_role('ROLE_USER')")
	 */
	public function submitAction(Request $request)
	{
		$concept = new Concept();
		$form = $this->get('form.factory')->create(ConceptType::class, $concept);

		if ($request->isMethod('POST') && $form->handleRequest($request)->isValid() && $request->get('g-recaptcha-response') != '')
		{
			$em = $this->getDoctrine()->getManager();
			$em->persist($concept);
			$em->flush();

			$em = $this->getDoctrine()->getManager()->getRepository('WCDCConceptBundle:Image');

			$last = $em->findOneBy(array(), array('id' => 'DESC'));
			$last_id = $last->getId();

			$beforeLast = $em->find($last_id - 1);

//			$images_ext = array('image/jpg', 'image/jpeg', 'image/png', 'image/gif');
//			$videos_ext = array('video/mp4', 'video/wmv', 'video/avi', 'video/mov', 'video/mpeg4', 'video/flv');

			if ($request->get('wc_img_crop') != '') {

				$wc_path_img_crop = preg_split('/\//', $request->get('wc_img_crop'));
				$wc_path_img_crop = './' . $wc_path_img_crop[3] . '/' . $wc_path_img_crop[4] . '/' . $wc_path_img_crop[5] . '/' . $wc_path_img_crop[6];

				$beforeLast_id = $last_id - 1;

				$wc_img_crop_new_path = './uploads/img/' . $beforeLast_id . '.' . $beforeLast->getUrl();

				rename($wc_path_img_crop, $wc_img_crop_new_path);
			}

			if ($request->get('dc_img_crop') != '') {

				$dc_path_img_crop = preg_split('/\//', $request->get('dc_img_crop'));
				$dc_path_img_crop = './' . $dc_path_img_crop[3] . '/' . $dc_path_img_crop[4] . '/' . $dc_path_img_crop[5] . '/' . $dc_path_img_crop[6];

				$dc_img_crop_new_path = './uploads/img/' . $last_id . '.' . $last->getUrl();

				rename($dc_path_img_crop, $dc_img_crop_new_path);
			}


			$request->getSession()->getFlashBag()->add('info', 'Your WCDC has been saved successfully');

			return $this->redirectToRoute('wcdc_core_home', array('id' => $concept->getId()));
		}

		return $this->render('WCDCConceptBundle:Concept:submit.html.twig', array('form' => $form->createView()));
	}

	public function uploadAction(Request $request)
	{
		if ($request->isXmlHttpRequest()) {
			$images_ext = array('image/jpg', 'image/jpeg', 'image/png', 'image/gif');
			$videos_ext = array('video/mp4', 'video/wmv', 'video/avi', 'video/mov', 'video/mpeg4', 'video/flv');
			$media = $request->files->get('concept');

			if ($request->get('wc_win_width'))
				$win_width = $request->get('wc_win_width');
			else if ($request->get('dc_win_width'))
				$win_width = $request->get('dc_win_width');

			if ($media['wcImg']['file'] != '' && $request->get('working') == '1') {
				$media = $media['wcImg']['file'];
			}
			else if ($media['dcImg']['file'] != '' && $request->get('dying') == '1') {
				$media = $media['dcImg']['file'];
			}

			if ($request->get('dying') == '1')
				$dying = 1;
			else
				$dying = 0;

			$images = new Image();
			$error = '';
			if ($media instanceof UploadedFile && $media->getError() == '0') {
				if ($media->getSize() < 3145728) {
					$originalName = $media->getClientOriginalName();
					$ext = $media->getMimeType();
					if (in_array($ext, $images_ext)) {
						$media->move($images->getUploadRootDir() . '/tmp', $originalName);

						$TailleImageChoisie = getimagesize($images->getUploadDir() . '/tmp/' . $originalName);
						$NouvelleLargeur = (int)$win_width / 4;
						$Reduction = (($NouvelleLargeur * 100)/$TailleImageChoisie[0]);
						$NouvelleHauteur = (($TailleImageChoisie[1] * $Reduction)/100);

						if ($ext === 'image/jpg' || $ext === 'image/jpeg') {
							$imgChoose = imagecreatefromjpeg($images->getUploadDir() . '/tmp/' . $originalName);
							$NouvelleImage = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur);
							imagecopyresampled($NouvelleImage, $imgChoose, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);
							imagedestroy($imgChoose);
							$NomImageChoisie = explode('.', $originalName);
							$NomImageExploitable = $NomImageChoisie[0] . '_' . time();
							$ext = $media->getClientOriginalExtension();
							$new_name = $images->getUploadDir() .'/tmp/'.$NomImageExploitable.'.'.$ext;
							imagejpeg($NouvelleImage , $new_name, 100);
						}
						else if ($ext === 'image/png') {
							$imgChoose = imagecreatefrompng($images->getUploadDir() . '/tmp/' . $originalName);
							$NouvelleImage = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur);
							imagecopyresampled($NouvelleImage, $imgChoose, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);
							imagedestroy($imgChoose);
							$NomImageChoisie = explode('.', $originalName);
							$NomImageExploitable = $NomImageChoisie[0] . '_' . time();
							$ext = $media->getClientOriginalExtension();
							$new_name = $images->getUploadDir() .'/tmp/'.$NomImageExploitable.'.'.$ext;
							imagepng($NouvelleImage , $new_name);
						}
						else if ($ext === 'image/gif') {
							$imgChoose = imagecreatefromgif($images->getUploadDir() . '/tmp/' . $originalName);
							$NouvelleImage = imagecreatetruecolor($NouvelleLargeur, $NouvelleHauteur);
							imagecopyresampled($NouvelleImage, $imgChoose, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $TailleImageChoisie[0], $TailleImageChoisie[1]);
							imagedestroy($imgChoose);
							$NomImageChoisie = explode('.', $originalName);
							$NomImageExploitable = $NomImageChoisie[0] . '_' . time();
							$ext = $media->getClientOriginalExtension();
							$new_name = $images->getUploadDir() .'/tmp/'.$NomImageExploitable.'.'.$ext;
							imagegif($NouvelleImage , $new_name);
						}
					}

					else if (in_array($ext, $videos_ext)) {
						$rootDir = __DIR__.'/../../../../web/uploads/video';
//						$uploadsDir = "uploads/video/";

						$media->move($rootDir, $originalName);
						$path = "/WCDC/web/uploads/video/" . $originalName;

						return $this->render('WCDCConceptBundle:Upload:thumbnailVideo.html.twig', array('path' => $path));
					}
					else {
						$error = "Invalid file type, please upload an image (.jpg, jpeg, .gif or .png) or an video (.mp4, .wmv, .avi, .mov, .mpeg4 or .flv) (video max duration 10 secondes) (file max size 3Mo)";
					}
				}
			}
			else {
				$error = "File is to large";
			}
			if (isset($new_name)) {
				$path = '/WCDC/web/' . $new_name;
				return $this->render('WCDCConceptBundle:Upload:thumbnail.html.twig', array('path' => $path, 'error' => $error, 'dying' => $dying));
			}
		}
	}

	public function cropAction(Request $request)
	{

		if ($request->isXmlHttpRequest()) {

			$dying = 0;
			if ($request->get('w') != '' && $request->get('working') == '1') {
//				echo "working";
				$x = $request->get('x');
				$y = $request->get('y');
				$w = $request->get('w');
				$h = $request->get('h');
				$wc_or_dc_image = $request->get('wc_image');
				$dying = 0;
			}
			elseif ($request->get('w1') != '' && $request->get('dying') == '1') {
//				echo "dying";
				$x = $request->get('x1');
				$y = $request->get('y1');
				$w = $request->get('w1');
				$h = $request->get('h1');
				$wc_or_dc_image = $request->get('dc_image');
				$dying = 1;
			}


			if (isset($x, $y, $w, $h, $wc_or_dc_image)) {

				$image = $request->files->get('concept');
				if ($image['wcImg']['file'] != '' && $dying == '0')
					$image = $image['wcImg']['file'];
				elseif ($image['dcImg']['file'] != '' && $dying == '1')
					$image = $image['dcImg']['file'];

				if ($image instanceof UploadedFile && $image->getError() == '0') {
					$ext = $image->getClientOriginalExtension();
					$tmp = preg_split('/\//', $wc_or_dc_image);
					$orginalName = $tmp[6];
					$tmp = new Image();
					$name = './' . $tmp->getUploadDir() . '/tmp/' . $orginalName;
//					echo "<br/>" . $name . "<br/>" . $orginalName . "<br/>" . $ext . "<br/>";
				}
				if (file_exists($name) && isset($image, $ext, $orginalName)) {
					if ($ext == "png")
						$source = imagecreatefrompng($name);
					else if ($ext == "jpg" || $ext == "jpeg")
						$source = imagecreatefromjpeg($name);
					else if ($ext == "gif")
						$source = imagecreatefromgif($name);
					else
						$error = "wrong pictures format";

					$arr = array('x' => $x, 'y' => $y, 'width' => $w, 'height' => $h);

					if (isset($source)) {

						$dst = imagecrop($source, $arr);
						$time = time();
						$tmp = explode('.', $orginalName);
						$names = $tmp[0] . '_' . $time . '.' . $ext;
						$path = "uploads/img/crop/";
						if (!file_exists($path))
							mkdir($path);
						$img_crop = '';
						if ($ext == "png") {
							imagepng($dst, $path . $names, null);
							$img_crop = '/WCDC/web/' . $path . $names;
						} else if ($ext == "jpg" || $ext == "jpeg") {
							imagejpeg($dst, $path . $names, 100);
							$img_crop = '/WCDC/web/' . $path . $names;
						} else if ($ext == "gif") {
							imagegif($dst, $path . $names, null);
							$img_crop = '/WCDC/web/' . $path . $names;
						}
						if ($img_crop) {
							unlink($name);
							imagedestroy($source);
							imagedestroy($dst);
							return $this->render('WCDCConceptBundle:Crop:croped.html.twig', array('img_crop' => $img_crop, 'error' => $error, 'dying' => $dying));
						}
					}
				}
			}
		}
	}
}