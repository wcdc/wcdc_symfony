<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 8/13/16
 * Time: 9:03 AM
 */

// src/WCDC/UserBundle/Entity/User.php

namespace WCDC\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="WCDC\UserBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
	/**
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;
}