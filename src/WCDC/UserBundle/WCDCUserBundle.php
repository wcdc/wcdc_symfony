<?php

namespace WCDC\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class WCDCUserBundle extends Bundle
{
	public function getParent()
	{
		return 'FOSUserBundle';
	}
}
