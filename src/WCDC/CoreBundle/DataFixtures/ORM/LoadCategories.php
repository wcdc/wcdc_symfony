<?php
// src/OC/PlatformBundle/DataFixtures/ORM/LoadCategories.php

namespace OC\PlatformBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use WCDC\CoreBundle\Entity\Categories;

class LoadCategories implements FixtureInterface
{
	// Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
	public function load(ObjectManager $manager)
	{
		// Liste des noms de catégorie à ajouter
		$names = array(
			'Politics',
			'Food',
			'Fashion',
			'Hotel',
			'Street Fashion',
			'Architecture',
			'Film',
			'Music',
			'Technology',
			'City',
			'Car',
			'Motorcycle',
			'Hair Style',
			'Sport',
			'Job',
			'Art',
			'Blog',
			'Organisation',
			'Event',
			'Brand',
			'Medicine',
			'Service',
		);

		foreach ($names as $name) {
			// On crée la catégorie
			$category = new Categories();
			$category->setName($name);

			// On la persiste
			$manager->persist($category);
		}

		// On déclenche l'enregistrement de toutes les catégories
		$manager->flush();
	}
}
