<?php
/**
 * Created by PhpStorm.
 * User: aaverty
 * Date: 8/17/16
 * Time: 2:41 PM
 */

namespace OC\PlatformBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use WCDC\CoreBundle\Entity\Cities;

class LoadCities implements FixtureInterface
{
	public function load(ObjectManager $manager)
	{
		// Liste des noms de catégorie à ajouter
		$names = array(
			'Paris',
			'Londres',
			'Berlin',
			'Madrid',
			'Rome',
			'Milan',
			'Bruxelle',
			'Lisbonne',
			'Amsterdam',
			'Zurich',
			'Washington DC',
			'Washington',
			'New York',
			'Los Angeles',
			'Miami',
			'San Francisco',
			'Chicago',
			'Toronto',
			'Le Caire',
			'Marrakech',
			'Sydney',
			'Tunis',
		);

		foreach ($names as $name) {
			// On crée la catégorie
			$city= new Cities();
			$city->setName($name);

			// On la persiste
			$manager->persist($city);
		}

		// On déclenche l'enregistrement de toutes les catégories
		$manager->flush();
	}
}