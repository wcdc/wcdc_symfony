<?php

namespace WCDC\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class CoreController extends Controller
{
	public function HomeAction()
	{
		$limit = 21;
		$conceptRepository = $this->getDoctrine()->getManager()->getRepository('WCDCConceptBundle:Concept');
		$conceptList = $conceptRepository->findBy(array(), array('datePublished' => 'DESC'),$limit);

		return $this->render('WCDCCoreBundle:Home:home.html.twig', array('concepts' => $conceptList));
	}


	public function The_ConceptAction()
	{
		return $this->render('WCDCCoreBundle:TheConcept:the_concept.html.twig');
	}

	public function ContactAction(Request $request)
	{
		if ($request->isMethod('POST'))
		{
			if (isset($_POST['Nom'], $_POST['Prenom'], $_POST['Mail'], $_POST['Message'], $_POST['g-recaptcha-response']))
			{
				$subject = "Contact from WCDC";
				$mail = filter_input(INPUT_POST, 'Mail', FILTER_SANITIZE_EMAIL);
				$mail = filter_var($mail, FILTER_VALIDATE_EMAIL);
				$msg = filter_input(INPUT_POST, 'Message', FILTER_SANITIZE_STRING);
				$name = filter_input(INPUT_POST, 'Nom', FILTER_SANITIZE_STRING);
				$firstname = filter_input(INPUT_POST, 'Prenom', FILTER_SANITIZE_STRING);
				$mess = "Bonjour\n\n" . $msg . "\n\nCordialement\n" . $name . " " . $firstname;
				if (isset($_POST['Telephone']))
				{
					$phone = filter_input(INPUT_POST, 'Telephone', FILTER_SANITIZE_NUMBER_INT);
					$mess .= " reply to : " . $mail . " or " . $phone;
				}
				else
				{
					$phone = null;
					$mess .= " reply to : " . $mail;
				}

				$message = \Swift_Message::newInstance()
					->setSubject($subject)
					->setFrom($mail)
					->setTo('aaverty69@gmail.com')
					->setBody($mess);
				$this->get('mailer')->send($message);

				$request->getSession()->getFlashBag()->add('info', 'Your contact mail was successfully sent. Thank you!');

				// Redirect - This is important to prevent users re-posting
				// the form if they refresh the page
				return $this->redirectToRoute('wcdc_core_home');
			}
		}

		return $this->render('WCDCCoreBundle:Contact:contact.html.twig');
	}

	public function menuAction () {
		$categories = $this->getDoctrine()->getRepository('WCDCCoreBundle:Categories')->findAll();
		$cities = $this->getDoctrine()->getRepository('WCDCCoreBundle:Cities')->findAll();
		return $this->render('WCDCCoreBundle:Menu:menu.html.twig', array('categories' => $categories, 'cities' => $cities));
	}

	public function testAction()
	{
		return $this->render('WCDCCoreBundle:Test:test.html.twig');
	}

}